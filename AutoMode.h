

void Auto_Loop(bool isNear, bool isAuto,int trainPin ,int barricadePin, int lightPin, int tunelRedPin, int tunelGreenPin) {


  switch (isAuto) {
            digitalWrite(trainPin, HIGH);
    case true:
      if (isNear) {
        digitalWrite(barricadePin, HIGH);
        digitalWrite(lightPin, HIGH);
        digitalWrite(tunelGreenPin, HIGH);
        digitalWrite(tunelRedPin, LOW);
      } else {
        digitalWrite(barricadePin, LOW);
        digitalWrite(lightPin, LOW);
        digitalWrite(tunelGreenPin, LOW);
        digitalWrite(tunelRedPin, HIGH);
      };
  }
}