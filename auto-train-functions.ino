#include "DistanceSensor.h"
#include "LCDButtons.h"
#include "LCDDisplay.h"
#include "RelayManager.h"
#include "AutoMode.h"
#include "SpeedCalculation.h"

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  initialize_screen();
  setup_relays();
  setup_distanceSensor();
}

void loop() {
  
  loop_distanceSensor();
  CalculateSpeed(isItNear);

  GetKey(workSpaceINT);
  isRelayOnScreen = IsRelayActiveOnScreen(workSpaceINT);

  currentActiveRelay = GetCurrentRelayOnScreen(workSpaceINT, isRelayOnScreen);
  currentRelayValue = GetCurrentRelayValue(currentActiveRelay, isRelayOnScreen);




  Screen_loop(workSpaceINT, currentActiveRelay, currentRelayValue,isRelayOnScreen, distanceInCm, speed);
  RelayAgragator(isRelayOnScreen, currentRelayValue, RelayPins[currentActiveRelay]);

  Auto_Loop(isItNear, isAuto, RelayPins[0], RelayPins[1], RelayPins[2], RelayPins[3], RelayPins[4]);

}
