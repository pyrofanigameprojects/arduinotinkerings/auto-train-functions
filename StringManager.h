#include "HardwareSerial.h"
#include "WString.h"

String displayUpStrings[9] = { "Mode", "Distance","Speed", "Relay 1", "Relay 2  ", "Relay 3  ", "Relay 4 ", "Relay 5  ", "Relay 6" };
String displayDownStrings[9] = { " ", "0.0","0.0", "off", "off", "off", "off", "off", "off" };



String isItOn(bool value) {
  if (!value)
    return "On";
  else return "Off";
}

String whatModIsIt(bool value) {
  if (value)
    return "Auto";
  else return "Manual";
}

String FlToStr(float value) {
  return String(value, 2);
}
void ModeSpecificStringChanges(int workSpaceINT,int relaysInt,bool relays, bool isRelayOnScreen, float distance,float speed) {
  if (isRelayOnScreen){
 
displayDownStrings[workSpaceINT] = isItOn(relays);
  }
    
    switch (workSpaceINT){
      case 0:
        displayDownStrings[0]=whatModIsIt(isAuto)
      ;
      case 1:
    displayDownStrings[1] = FlToStr(distance) +" CM"
      ;
      case 2:
    displayDownStrings[2] = FlToStr(speed) +" CM/S"
      ;
    } 
}