#include "HardwareSerial.h"
#include "BoolSelector.h"
 
//Buttons
enum keys { none,
            selection,
            left,
            down,
            up,
            right
};

const int button_id = A0;
int screenButtonsPinAvrg = 0;
keys currentKey = none;
String currentKeyName = "none";

#include "WorkSpaceManager.h"




void GetKey(int workspaceIndex) {
  screenButtonsPinAvrg = analogRead(button_id);
  if (screenButtonsPinAvrg >= 818)  ///nothing from4 volts and up
  {
    currentKey = none;
    currentKeyName = "none";
  }
  if (630 <= screenButtonsPinAvrg && screenButtonsPinAvrg < 800) {
    currentKey = selection;
    currentKeyName = "selection";

    isAuto = !isAuto;

    currentKey = none;
  }
  if (400 <= screenButtonsPinAvrg && 600 > screenButtonsPinAvrg) {
    currentKey = left;
    currentKeyName = "left";
        workSpaceINT = workSpacesIndex(workSpaceINT, isAuto);

    currentKey = none;
  }
  if (204 <= screenButtonsPinAvrg && screenButtonsPinAvrg < 300) {
    currentKey = down;
    currentKeyName = "down";
    
    ModeSpecificBooleanChanges(workspaceIndex);

     currentKey = none;
  }
  if (90 <= screenButtonsPinAvrg && screenButtonsPinAvrg < 150) {
    currentKey = up;
    currentKeyName = "up";
    
    ModeSpecificBooleanChanges(workspaceIndex);
    currentKey = none;
  }
  if (screenButtonsPinAvrg < 61) {
    currentKey = right;
    currentKeyName = "right";
    workSpaceINT = workSpacesIndex(workSpaceINT, isAuto);
    currentKey = none;
  }
  delay(150);
}
